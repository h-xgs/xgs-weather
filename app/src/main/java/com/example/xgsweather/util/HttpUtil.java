package com.example.xgsweather.util;

import okhttp3.OkHttpClient;
import okhttp3.Request;

/**
 * 由于OkHttp的封装，这里和服务器进行交互的代码非常简单.
 * 现在发起一条HTTP请求只需要调用sendOkHttpRequest()方法，传入请求地址，并注册一个回调来处理服务器响应就可以了。
 */
public class HttpUtil {

    /**
     * 必应背景图片的地址接口
     */
    public static final String requestBingPic = "http://api.ee123.net/img/bingimg/dayimg.jpg?d=20230922";

    // 向服务器发送请求，响应的数据会回调到onResponse()方法中
    public static void sendOkHttpRequest(String address, okhttp3.Callback callback) {
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder().url(address).build();
        client.newCall(request).enqueue(callback);
    }
}
