package com.example.xgsweather.util;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.widget.Toast;

import java.util.Arrays;
import java.util.List;
import java.util.Locale;

public class LocationUtil {

    // 获取经纬度
    public static String autoGetLocation(Context context) {
        if ((context.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) &&
                (context.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {
            return "";
        }
        LocationManager locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(false);
        String locationProvider = null;
        if (providers.contains(LocationManager.GPS_PROVIDER)) {
            //如果是GPS
            locationProvider = LocationManager.GPS_PROVIDER;
            LogUtil.d("定位方式GPS");
        } else if (providers.contains(LocationManager.NETWORK_PROVIDER)) {
            //如果是Network
            locationProvider = LocationManager.NETWORK_PROVIDER;
            LogUtil.d("定位方式Network");
        } else {
            Toast.makeText(context, "没有可用的位置提供器", Toast.LENGTH_SHORT).show();
            LogUtil.d("没有可用的位置提供器");
        }
        @SuppressLint("MissingPermission")
        Location location = locationManager.getLastKnownLocation(locationProvider);
        LogUtil.d("location：" + location);
        if (location != null) {
            LogUtil.d(location.toString());
            LogUtil.d("获取上次的位置-经纬度：" + location.getLongitude() + " " + location.getLatitude());
            try {
                return getAddress(location, context).get(0).getSubLocality();
            } catch (Exception e) {
                LogUtil.e(e.getMessage());
            }
        }
        return "";
    }

    //获取地址信息:城市、街道等信息
    private static List<Address> getAddress(Location location, Context c) {
        List<Address> result = null;
        try {
            if (location != null) {
                Geocoder gc = new Geocoder(c, Locale.getDefault());
                result = gc.getFromLocation(location.getLatitude(), location.getLongitude(), 1);
                LogUtil.d("获取地址信息：" + result.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    public static boolean checkAndBuildPermissions(Activity activity) {
        boolean requestInternetPermission = false;
        boolean requestFineLocationPermission = false;
        boolean requestCoarseLocationPermission = false;

        SharedPreferences prefs = activity.getSharedPreferences("app_settings", Context.MODE_PRIVATE);
        if (!prefs.getBoolean("isNeedCheckPermissions", true)) {
            return false;
        }

        int numPermissionsToRequest = 0;
        if (activity.checkSelfPermission(Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            requestInternetPermission = true;
            numPermissionsToRequest++;
        }
        if (activity.checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestFineLocationPermission = true;
            numPermissionsToRequest++;
        }
        if (activity.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            requestCoarseLocationPermission = true;
            numPermissionsToRequest++;
        }

        if (!requestInternetPermission && !requestFineLocationPermission && !requestCoarseLocationPermission) {
            return false;
        }

        String[] permissionsToRequest = new String[numPermissionsToRequest];
        int permissionsRequestIndex = 0;
        if (requestInternetPermission) {
            permissionsToRequest[permissionsRequestIndex] = Manifest.permission.INTERNET;
            permissionsRequestIndex++;
        }
        if (requestFineLocationPermission) {
            permissionsToRequest[permissionsRequestIndex] = Manifest.permission.ACCESS_FINE_LOCATION;
            permissionsRequestIndex++;
        }
        if (requestCoarseLocationPermission) {
            permissionsToRequest[permissionsRequestIndex] = Manifest.permission.ACCESS_COARSE_LOCATION;
        }
        LogUtil.w("checkAndBuildPermissions: permissionsToRequest" + Arrays.toString(permissionsToRequest));
        activity.requestPermissions(permissionsToRequest, 200);
        return true;
    }

}
