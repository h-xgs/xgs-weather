package com.example.xgsweather.util;

import android.content.Context;
import android.text.TextUtils;

import com.example.xgsweather.db.City;
import com.example.xgsweather.db.County;
import com.example.xgsweather.db.Province;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.qweather.sdk.bean.geo.GeoBean;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * 数据解析工具类，解析处理JSON格式的省市县数据并存入数据库。
 */
public class HandleResponseUtil {

    /**
     * 将 provinces.json资源文件转换为json字符串
     */
    private static String loadProvinceJson(Context context) {
        String fileName = "provinces.json";
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream inputStream = context.getResources().getAssets().open(fileName);
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    /**
     * 解析处理省级json字符串数据
     */
    public static void handleProvinceResponse(Context context) {
        String response = loadProvinceJson(context);
        if (!TextUtils.isEmpty(response)) {
            List<Province> allProvinces = new Gson().fromJson(response, new TypeToken<List<Province>>() {
            }.getType());
            for (int i = 0; i < allProvinces.size(); i++) {
                Province province = allProvinces.get(i);
                // 调用save()方法将数据存储到数据库当中
                province.save();
            }
        }
    }

    /**
     * 解析处理服务器返回的市级数据
     */
    public static boolean handleCityResponse(List<GeoBean.LocationBean> locationBeanList, int provinceId) {
        if (!locationBeanList.isEmpty()) {
            for (int i = 0; i < locationBeanList.size(); i++) {
                String cityName = locationBeanList.get(i).getName();
                String cityCode = locationBeanList.get(i).getId();
                //将每个对象保存到数据库中
                City city = new City();
                city.setCityName(cityName);
                city.setCityCode(cityCode);
                city.setProvinceId(provinceId);
                city.save();
            }
            return true;
        }
        return false;
    }

    /**
     * 解析处理服务器返回的县级数据
     */
    public static boolean handleCountyResponse(List<GeoBean.LocationBean> locationBeanList, int cityId) {
        if (!locationBeanList.isEmpty()) {
            for (int i = 0; i < locationBeanList.size(); i++) {
                String countyName = locationBeanList.get(i).getName();
                String countyCode = locationBeanList.get(i).getId();
                //将每个对象保存到数据库中
                County county = new County();
                county.setCountyName(countyName);
                county.setCountyCode(countyCode);
                county.setCityId(cityId);
                county.save();
            }
            return true;
        }
        return false;
    }

}
