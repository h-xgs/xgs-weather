package com.example.xgsweather.db;

import com.google.gson.annotations.SerializedName;

import org.litepal.crud.LitePalSupport;

/**
 * 省的数据信息
 * LitePal中的每一个实体类都是必须要继承自DataSupport类
 */
public class Province extends LitePalSupport {
    private int id;
    // 省的名字
    @SerializedName("name")
    private String provinceName;
    // 省的代号
    @SerializedName("code")
    private int provinceCode;

    public Province() {
    }

    public Province(String provinceName, int provinceCode) {
        this.provinceName = provinceName;
        this.provinceCode = provinceCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getProvinceName() {
        return provinceName;
    }

    public void setProvinceName(String provinceName) {
        this.provinceName = provinceName;
    }

    public int getProvinceCode() {
        return provinceCode;
    }

    public void setProvinceCode(int provinceCode) {
        this.provinceCode = provinceCode;
    }
}
