package com.example.xgsweather.db;

import org.litepal.crud.LitePalSupport;

/**
 * 县的数据信息
 */
public class County extends LitePalSupport {
    private int id;
    // 县的名字
    private String countyName;
    // 县所对应的天气id
    private String countyCode;
    // 当前县所属市的id值
    private int cityId;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCountyName() {
        return countyName;
    }

    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    public String getCountyCode() {
        return countyCode;
    }

    public void setCountyCode(String countyCode) {
        this.countyCode = countyCode;
    }

    public int getCityId() {
        return cityId;
    }

    public void setCityId(int cityId) {
        this.cityId = cityId;
    }
}
