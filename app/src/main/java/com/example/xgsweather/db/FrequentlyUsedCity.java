package com.example.xgsweather.db;

import androidx.annotation.NonNull;

import org.litepal.crud.LitePalSupport;

/**
 * 常用城市列表
 */
public class FrequentlyUsedCity extends LitePalSupport {
    // 天气id
    private String weatherId;
    // 城市的名字
    private String cityName;

    public FrequentlyUsedCity() {
    }

    public FrequentlyUsedCity(String mWeatherId, String mCountyName) {
        this.weatherId = mWeatherId;
        this.cityName = mCountyName;
    }

    public String getWeatherId() {
        return weatherId;
    }

    public void setWeatherId(String weatherId) {
        this.weatherId = weatherId;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @NonNull
    @Override
    public String toString() {
        return "FrequentlyUsedCity{" +
                "weatherId='" + weatherId + '\'' +
                ", cityName='" + cityName + '\'' +
                '}';
    }

}
