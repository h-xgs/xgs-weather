package com.example.xgsweather.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.xgsweather.R;
import com.example.xgsweather.adapter.CitiesAdapter;
import com.example.xgsweather.db.FrequentlyUsedCity;

import org.litepal.LitePal;

import java.util.List;

public class ManageCitiesActivity extends BaseActivity {
    private boolean isAddMod = false;
    private boolean isDelMod = false;
    private LinearLayout layoutManageCities;
    private LinearLayout layoutChooseArea;
    private Button actionButton;
    private List<FrequentlyUsedCity> frequentlyUsedCities;
    private CitiesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_cities);

        RecyclerView recyclerView = findViewById(R.id.city_list);
        frequentlyUsedCities = LitePal.findAll(FrequentlyUsedCity.class);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CitiesAdapter(frequentlyUsedCities, this, isDelMod);
        recyclerView.setAdapter(adapter);

        layoutManageCities = findViewById(R.id.layout_activity_manage_cities);
        layoutChooseArea = findViewById(R.id.layout_choose_area_fragment);
        actionButton = findViewById(R.id.ok_del_button);
        actionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isDelMod) {
                    isDelMod = false;
                    actionButton.setBackgroundResource(R.drawable.ic_to_del);
                } else {
                    isDelMod = true;
                    actionButton.setBackgroundResource(R.drawable.ic_ok);
                }
                adapter.setDelMode(isDelMod);
                frequentlyUsedCities = LitePal.findAll(FrequentlyUsedCity.class);
                adapter.notifyDataSetChanged();
            }
        });

        Button addButton = findViewById(R.id.add_city_button);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isAddMod = true;
                layoutManageCities.setVisibility(View.GONE);
                layoutChooseArea.setVisibility(View.VISIBLE);
            }
        });

        Button mBackButton = findViewById(R.id.menu_back_button);
        mBackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (isAddMod) {
            isAddMod = false;
            layoutManageCities.setVisibility(View.VISIBLE);
            layoutChooseArea.setVisibility(View.GONE);
            return;
        }
        if (isDelMod) {
            isDelMod = false;
            actionButton.setBackgroundResource(R.drawable.ic_to_del);
            adapter.setDelMode(isDelMod);
            frequentlyUsedCities = LitePal.findAll(FrequentlyUsedCity.class);
            adapter.notifyDataSetChanged();
            return;
        }
        startActivity(new Intent(this, MainActivity.class));
        super.onBackPressed();
    }

}