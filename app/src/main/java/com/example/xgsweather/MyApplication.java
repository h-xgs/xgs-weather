package com.example.xgsweather;

import android.app.Application;

import com.qweather.sdk.view.HeConfig;

import org.litepal.LitePal;

public class MyApplication extends Application {

    /**
     * 和风天气的 PublicId 和 key ,在和风天气控制台获取。
     */
    public static final String PUBLIC_ID = "HE2209120953151797";
    public static final String KEY = "c641866fdd2943419a99fff155769523";

    @Override
    public void onCreate() {
        super.onCreate();
        LitePal.initialize(this);

        // 进行账户初始化（全局执行一次即可）
        HeConfig.init(PUBLIC_ID, KEY);
        // 切换至免费订阅（全局执行一次即可）
        HeConfig.switchToDevService();
    }

}
